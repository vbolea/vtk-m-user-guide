\chapter{File I/O}
\label{chap:FileIO}

\index{I/O|(}
\index{file I/O|(}

Before \VTKm can be used to process data, data need to be loaded into the
system. \VTKm comes with a basic file I/O package to get started developing
very quickly. All the file I/O classes are declared under the \vtkmio{}
namespace.

\begin{didyouknow}
  Files are just one of many ways to get data in and out of \VTKm.
  In later chapters we explore ways to define \VTKm data structures of increasing power and complexity.
  In particular, Section \ref{sec:DataSets:Building} describes how to build \VTKm data set objects and Section \ref{sec:ArrayHandle:Adapting} documents how to adapt data structures defined in other libraries to be used directly in \VTKm.
\end{didyouknow}

\section{Readers}

\index{file I/O!read|(}
\index{read file|(}

All reader classes provided by \VTKm are located in the \vtkmio{}
namespace. The general interface for each reader class is to accept a
filename in the constructor and to provide a \textcode{ReadDataSet} method
to load the data from disk.

The data in the file are returned in a \vtkmcont{DataSet} object as described in Chapter~\ref{chap:DataSets}, but it is sufficient to known that a \textidentifier{DataSet} can be passed around readers, writers, filters, and rendering units.

\subsection{Legacy VTK File Reader}

Legacy VTK files are a simple open format for storing visualization data.
These files typically have a \textfilename{.vtk} extension. Legacy VTK
files are popular because they are simple to create and read and are
consequently supported by a large number of tools. The format of legacy VTK
files is well documented in \textit{The VTK User's Guide}.\footnote{A free
  excerpt describing the file format is available at
  \url{http://www.vtk.org/Wiki/File:VTK-File-Formats.pdf}.} Legacy VTK
files can also be read and written with tools like ParaView and VisIt.

Legacy VTK files can be read using the \vtkmio{VTKDataSetReader}
class. The constructor for this class takes a string containing the
filename. The \classmember*{VTKDataSetReader}{ReadDataSet} method reads the data from the
previously indicated file and returns a \vtkmcont{DataSet} object, which
can be used with filters and rendering.

\vtkmlisting{Reading a legacy VTK file.}{VTKDataSetReader.cxx}

\subsection{Image Readers}

\VTKm provides classes to read images from some standard image formats.
These readers will store the data in a \vtkmcont{DataSet} object with the colors stored as a named point field.
The colors are read as 4-component RGBA vectors for each pixel.
Each component in the pixel color is stored as a 32-bit float between 0 and 1.

Portable Network Graphics (PNG) files can be read using the \vtkmio{ImageReaderPNG} class.
A \textidentifier{ImageReaderPNG} object is constructed with the name of the file to load.
The data from the file are loaded using the \classmember*{ImageReaderPNG}{ReadDataSet} method, which returns a \textidentifier{DataSet} object.
By default, the colors are stored in a field named ``colors'', but the name of the field can optionally be changed using the \classmember*{ImageReaderPNG}{SetPointFieldName} method.

\vtkmlisting{Reading an image from a PNG file.}{ImageReaderPNG.cxx}

Portable anymap (PNM) files can be read using the \vtkmio{ImageReaderPNM} class.
Currently, the PNM file reader only supports files using the portable pixmap (PPM) format (with magic number ``P6'').
These files are most commonly stored with a \textfilename{.ppm} extension although the \textfilename{.pnm} extension is also valid.
Like for PNG files, a \textidentifier{ImageReaderPNM} is constructed with the name of the file to read from.
\textidentifier{ImageReaderPNM} also uses the same \classmember*{ImageReaderPNM}{ReadDataSet} and optional \classmember*{ImageReaderPNM}{SetPointFieldName} methods.

\vtkmlisting{Reading an image from a PNM file.}{ImageReaderPNM.cxx}

\index{read file|)}
\index{file I/O!read|)}

\section{Writers}

\index{file I/O!write|(}
\index{write file|(}

All writer classes provided by \VTKm are located in the \vtkmio{}
namespace. The general interface for each writer class is to accept a
filename in the constructor and to provide a \textcode{WriteDataSet} method
to save data to the disk. The \textcode{WriteDataSet} method takes a
\vtkmcont{DataSet} object as an argument, which contains the data to write
to the file.

\subsection{Legacy VTK File Writer}

\index{write file|)}
\index{file I/O!write|)}

Legacy VTK files can be written using the \vtkmio{VTKDataSetWriter}
class. The constructor for this class takes a string containing the
filename. The \classmember*{VTKDataSetWriter}{WriteDataSet} method takes a \vtkmcont{DataSet}
object and writes its data to the previously indicated file.

\vtkmlisting{Writing a legacy VTK file.}{VTKDataSetWriter.cxx}

\subsection{Image Writers}

\VTKm provides classes to some standard image formats.
These writers store data in a \vtkmcont{DataSet}.
The data must be a 2D structure with the colors stored in a point field.
(See Chapter \ref{chap:DataSet} for details on \textidentifier{DataSet} objects.)

Portable Network Graphics (PNG) files can be written using the \vtkmio{ImageWriterPNG} class.
A \textidentifier{ImageWriterPNG} object is constructed with the name of the file to save.
The data are written to the file using the \classmember*{ImageWriterPNG}{WriteDataSet} method.
This method takes a \textidentifier{DataSet} object as an argument.
An optional second argument can be given to name the field containing color data to write.
If the field is not given, the first point field of the appropriate type is used.

By default, PNG files are written as RGBA colors using 8-bits for each component.
You can change the format written using the \classmember*{ImageWriterPNG}{SetPixelDepth} method.
This takes an item in the \classmember{ImageWriterPNG}{PixelDepth} enumeration.
Valid values are
\begin{description}
\item[\classmember{PixelDepth}{PIXEL\_8}] %
  8-bit RGBA values (default).
\item[\classmember{PixelDepth}{PIXEL\_16}] %
  16-bit RGBA values.
\end{description}

\vtkmlisting{Writing an image to a PNG file.}{ImageWriterPNG.cxx}

Portable anymap (PNM) files can be written using the \vtkmio{ImageWriterPNM} class.
Currently, the PNM file writer only supports files using the portable pixmap (PPM) format (with magic number ``P6'').
These files are most commonly stored with a \textfilename{.ppm} extensions although the \textfilename{.pnm} extension is also valid.

Like for PNG files, a \textidentifier{ImageReaderPNM} is constructed with the name of the file to write.
\textidentifier{ImageReaderPNM} also uses the same \classmember*{ImageReaderPNM}{WriteDataSet} and \classmember*{ImageReaderPNM}{SetPixelDepth}.
It also sports the same \classmember{ImageReaderPNM}{PixelDepth} enumeration.

\vtkmlisting{Writing an image to a PNM file.}{ImageWriterPNM.cxx}

\index{file I/O|)}
\index{I/O|)}
