
set(example_src
  ArrayHandle.cxx
  ArrayHandleAdapt.cxx
  ArrayHandleCast.cxx
  ArrayHandleCompositeVector.cxx
  ArrayHandleConstant.cxx
  ArrayHandleCoordinateSystems.cxx
  ArrayHandleCounting.cxx
  ArrayHandleDecorated.cxx
  ArrayHandleDerived.cxx
  ArrayHandleDiscard.cxx
  ArrayHandleExtractComponent.cxx
  ArrayHandleGroupVec.cxx
  ArrayHandleImplicit.cxx
  ArrayHandlePermutation.cxx
  ArrayHandleRandomUniformBits.cxx
  ArrayHandleSwizzle.cxx
  ArrayHandleTransform.cxx
  ArrayHandleView.cxx
  ArrayHandleZip.cxx
  CellLocator.cxx
  CellEdgesFaces.cxx
  CellOperations.cxx
  CellShapes.cxx
  ColorTables.cxx
  CoreDataTypes.cxx
  CustomDeviceAdapter.cxx
  DataSetCreation.cxx
  DeviceAdapterTag.cxx
  DeviceAdapterAlgorithms.cxx
  EnvironmentModifierMacros.cxx
  ErrorHandling.cxx
  FilterDataSetWithField.cxx
  FractalWorklets.cxx
  FunctionInterface.cxx
  GenerateMeshCombineLike.cxx
  GenerateMeshConstantShape.cxx
  GenerateMeshHash.cxx
  GenerateMeshVariableShape.cxx
  Initialization.cxx
  IO.cxx
  Lists.cxx
  Matrix.cxx
  NewtonsMethod.cxx
  PointLocator.cxx
  PredicatesAndOperators.cxx
  ProvidedFilters.cxx
  Rendering.cxx
  RenderingInteractive.cxx
  RuntimeDeviceTracker.cxx
  ScatterCounting.cxx
  ScatterPermutation.cxx
  ScatterUniform.cxx
  SumOfAngles.cxx
  SimpleAlgorithm.cxx
  SimpleHistogram.cxx
  Timer.cxx
  Traits.cxx
  TransferringArguments.cxx
  TriangleQuality.cxx
  TryExecute.cxx
  Tuple.cxx
  UnknownArrayHandle.cxx
  UseWorkletMapField.cxx
  UseWorkletPointNeighborhood.cxx
  UseWorkletReduceByKey.cxx
  UseWorkletVisitCellsWithPoints.cxx
  UseWorkletVisitPointsWithCells.cxx
  )

# Set up compiling and testing of examples.
if (BUILD_EXAMPLES)
  # Special example that is an encapsulated program
  include(VTKmQuickStart.cmake)
  add_test(NAME VTKmQuickStart
    COMMAND VTKmQuickStart ${CMAKE_SOURCE_DIR}/data/cow.vtk
    )

  find_package(VTKm REQUIRED)

  vtkm_find_gl(REQUIRED GL GLUT)

  message(STATUS "VTK-m reported version: ${VTKm_VERSION_FULL}")
  if(VTKm_VERSION_FULL MATCHES "[0-9]+.[0-9]+.[0-9]+-.*")
    # If we are in between VTK-m versions, change the version to give the
    # commit we are currently documenting.
    set(VTKm_GUIDE_VERSION ${VTKm_VERSION_FULL} PARENT_SCOPE)
  else()
    # If we are at an actual release (as far as we can tell), make sure the
    # version matches up.
    if(NOT VTKm_GUIDE_VERSION STREQUAL VTKm_VERSION)
      message(SEND_ERROR "VTKm_GUIDE_VERSION seems to be set wrong. Change in CMakeLists.txt")
    endif()
  endif()

  set(test_prog ExampleTests)
  create_test_sourcelist(test_src ${test_prog}.cxx ${example_src})
  add_executable(${test_prog} ${test_src})
  target_link_libraries(${test_prog}
    vtkm::cont
    vtkm::rendering
    vtkm::cont_testing
    vtkm::filter
    OpenGL::GL
    GLUT::GLUT
    Threads::Threads
    )

  foreach (test ${example_src})
    get_filename_component(tname ${test} NAME_WE)
    set(test_args)
    if(tname STREQUAL "RenderingInteractive")
      set(test_args --no-interaction)
    endif()
    add_test(NAME ${tname}
      COMMAND ${test_prog} ${tname} ${test_args}
      )
  endforeach()

  if(NOT WIN32)
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E
        create_symlink ${CMAKE_SOURCE_DIR}/data ${CMAKE_CURRENT_BINARY_DIR}/data
      )
  else()
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E
        copy_directory ${CMAKE_SOURCE_DIR}/data ${CMAKE_CURRENT_BINARY_DIR}/data
      )
  endif()

  add_custom_target(examples)
  add_dependencies(examples VTKmQuickStart ${test_prog})
endif()

# Set up targets to extract examples from code
latex_get_output_path(output_dir)
set(scan_target_files)
foreach(src_file ${example_src} VTKmQuickStart.cxx)
  set(known_out_file ${output_dir}/${src_file}-scanned)
  add_custom_command(OUTPUT ${known_out_file}
    COMMAND ${CMAKE_COMMAND}
      -D INPUT_FILE=${CMAKE_CURRENT_SOURCE_DIR}/${src_file}
      -D OUTPUT_DIRECTORY=${output_dir}
      -P ${CMAKE_CURRENT_SOURCE_DIR}/ExtractExample.cmake
    COMMAND ${CMAKE_COMMAND} -E touch ${known_out_file}
    DEPENDS ${src_file} ExtractExample.cmake
    COMMENT "Extracting examples from ${src_file}"
    )
  list(APPEND scan_target_files ${known_out_file})
endforeach()

set(example_listings ${scan_target_files} PARENT_SCOPE)

# We need this target to make sure the targets for all the files in
# ${scan_target_files} (and ${example_listings}) will be created. In other
# directories you need to depend on both ${example_listings} and
# ExtractExamples to ensure that all the dependencies are their. (Ninja
# actually only needs ${example_listings}, but Unix Makefiles need both.)
add_custom_target(ExtractExamples
  DEPENDS ${scan_target_files}
  SOURCES ExtractExample.cmake
  )
